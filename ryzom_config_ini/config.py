import os
import sys
import glob
import json
import shutil
import random
import appdirs
import humanize
import platform
import traceback

from os import path as p

from kyssModule import KyssModule, KyssConfigIni
from kyssUtils import KyssUtils
k = KyssUtils("RyzomConfigIni")

INI_LAST_VERSION = 3

class RyzomConfigIni(KyssModule):

	def _init(self):
		self.servers = None
		self.profiles = None
		self.shortcuts = None
		self.ryzom_roaming_path = appdirs.user_data_dir("Ryzom", "", roaming=True)
		self.ryzom_user_path = appdirs.user_data_dir("Ryzom", "", roaming=False)
		self.config = KyssConfigIni(self.ryzom_roaming_path+"/ryzom.ini")
		self.version = int(self.config.get("latest", "version", "1"))
		if self.version < 3:
			self.config.reset()
			self.config.set("latest", "version", str(INI_LAST_VERSION))
			self.config.save()
	
		self.userInfos = self._getUserInfos()
	
	def _getUserInfos(self):
		token = self.getConfig("me", "token")
		userInfos = {}
		if token:
			infos = self.downloadFile("https://me.ryzom.com/api/oauth/user?access_token="+token)
			if infos:
				try:
					userInfos = json.loads(infos)
				except Exception as e:
					self.error(e)
					pass
		return userInfos
	
	def getUsedZones(self):
		return {"main": 500}

	def getShardName(self, name, default="Atys"):
		domains = {"ryzom_live" : "Atys", "ryzom_dev" : "Yubo", "ryzom_test" : "Gingo"}
		if name in domains:
			return domains[name]
		return default
	
	def getDomainName(self, name, default="ryzom_live"):
		shards = {"Atys": "ryzom_live", "Yubo": "ryzom_dev", "Gingo": "ryzom_test"}
		if name in shards:
			return shards[name]
		return default
		
	
	def getDomainVersionsFromPath(self, path):
		if p.exists(path+os.sep+"client_default.cfg") and p.exists(path+os.sep+"unpack"):
			versions = []
			for x in glob.glob(path+os.sep+"unpack"+os.sep+"ryzom_*.version"):
				with open(x) as f:
					versions.append((p.basename(x).replace(".version", ""), f.read()))
			return versions
		return None

	def getDomainFromClientCfg(self, filename, default="ryzom_default"):
		RyzomCfg = self.getModule("ryzom_cfg")
		cfg = RyzomCfg.getCfg(filename)
		if cfg.empty():
			return None
		try:
			domain = cfg.get("Application")[0]
		except:
			domain = default
		return domain

	def updatePathInformations(self, shard, selected_path, versions):
		if versions != None and p.isdir(selected_path):
			disk_usage = shutil.disk_usage(selected_path)
			total_space = humanize.naturalsize(disk_usage.total, binary=True)
			free_space = humanize.naturalsize(disk_usage.free, binary=True)

			if not versions:
				if p.isdir(selected_path+os.sep+"data") and p.isdir(selected_path+os.sep+"cfg") and p.isfile(selected_path+os.sep+"client_default.cfg"):
					self.setE("#-"+shard+"-message", self._("path_containts_ryzom_install"))
				else:
					self.setE("#-"+shard+"-message", self._("new_installation"))
			elif len(versions) == 1:
				version = versions[0]
				self.setE("#-"+shard+"-message", self._("path_with_ryzom", (version[0], version[1].split(" ")[0])))
			else:
				for version in versions:
					if self.getShardName(version[0]).lower() == shard:
						self.setE("#-"+shard+"-message", self._("path_contains_version", (version[0], version[1].split(" ")[0])))
						return
				self.setE("#-"+shard+"-message",  self._("path_contains_version", (version[0], version[1].split(" ")[0])))
		else:
			self.setE("#-"+shard+"-message", self._("new_installation"))
	
	def setMessage(self, message):
		if message:
			self.showE("#-message")
			self.setE("#-message", message)
			r = random.randint(108, 202);
			g = random.randint(108, 202);
			b = random.randint(108, 202);
			self.setupE("#-message", "style.backgroundColor", "\"rgb({},{},{})\"".format(r, g, b))
		else:
			self.hideE("#-message")

	def getServersFromIni(self):
		self.servers = {}
		sections = self.config.getSections()
		for shard in ("Atys", "Yubo", "Gingo"):
			if shard in sections:
				self.servers[shard] = self.config.get(shard, "location")
		return self.servers

	def getProfilesFromIni(self, skip_no_shortcuts=False):
		self.profiles = {}
		sections = self.config.getSections()
		shortcuts = {}
		for section in sections:
			if section[:9] == "shortcut_":
				profile = self.config.get(section, "profile")
				if not profile in shortcuts:
					shortcuts[profile] = 0
				shortcuts[profile] += 1

		
		for section in sections:
			if section[:8] == "profile_":
				profile = dict(self.config.get(section))
				if not "folder" in profile or not "name" in profile:
					self.config.remove(section)
					self.config.save()
					continue
					
				domain = self.getDomainFromClientCfg(self.ryzom_roaming_path+os.sep+profile["folder"]+os.sep+"client.cfg")
				self.log("Domain found at {} = {}".format(profile["folder"], domain))
				if not domain:
					if "shard" in profile:
						shard = profile["shard"]
					else:
						shard = "Atys"
					self.createProfileFolder(profile["folder"])
					self.updateShardInProfile(shard, profile["folder"])
					domain = self.getDomainName(shard)
				else:
					shard = self.getShardName(domain)
					if "shard" in profile and shard != profile["shard"]:
						self.updateShardInProfile(shard, profile["folder"]);

				#if skip_no_shortcuts and (not section in shortcuts or shortcuts[section] == 0):
				#	continue

				if not shard in self.profiles:
					self.profiles[shard] = {}
				
				self.profiles[shard][section] = (profile["name"], profile["folder"], shard)
		self.log("Found profiles: {}".format(self.profiles))
		self.log("USER INFOS : {}".format(self.userInfos))
		for shard in ("Atys", "Yubo", "Gingo"):
			if shard.lower() in self.userInfos and "name" in self.userInfos:
				name = "profile_"+shard+"_"+self.userInfos["name"]
				add_profile = True
				use_profile = ""
				self.log("PROFILES = {}".format(self.profiles))
				if shard in self.profiles:
					for pid, profile in self.profiles[shard].items():
						if len(profile) > 2 and profile[2] == shard:
							use_profile = pid
						self.log("Check {} == {}".format(pid, name))
						if pid == name:
							add_profile = False
				else:
					self.profiles[shard] = {}
				
				if add_profile:
					self.log("ADD PROFILES = {}".format(self.profiles))
					self.log("------------------------ ADD PROFILE --------------------- "+name+" "+shard)
					if shard == "Atys":
						folder = self.addDefaultProfile(name, self.userInfos["name"], shard)
					else:
						folder = self.addDefaultProfile(name, self.userInfos["name"]+" ("+shard+")", shard)
					self.profiles[shard][name] = (self.userInfos["name"]+" ("+shard+")", folder, shard)
			elif shard == "Atys":
				if not shard in self.profiles:
					self.profiles[shard] = {}
				folder = self.addDefaultProfile("atys", "Atys", shard)
				self.profiles[shard]["atys"] = ("Atys", folder, shard)
		self.log("Found profiles: {}".format(self.profiles))
		return self.profiles

	def getShortcutsFromIni(self, filter_profile="", filter_shard="", skip_disabled=False):
		self.userInfos = self._getUserInfos()
		save_config = False
		self.shortcuts = {}
		sections = self.config.getSections()
		for section in sections:
			if section[:9] == "shortcut_":
				profile = self.config.get(section, "profile")
				if not profile in self.shortcuts:
					self.shortcuts[profile] = {}
				shortcut = self.config.get(section)
				
				if not "name" in shortcut:
					continue
				
				if not "args" in shortcut:
					shortcut["args"] = ""
					
				if not "disabled" in shortcut:
					shortcut["disabled"] = "0"
				
				if skip_disabled:
					if shortcut["disabled"] == "1":
						continue
				self.shortcuts[profile][section] = (shortcut["name"], shortcut["args"], shortcut["disabled"] == "1")
		if filter_profile:
			if filter_profile in self.shortcuts:
				shortcuts = self.shortcuts[filter_profile]
			else:
				shortcuts = {}
			profile_infos = self.config.get(filter_profile)
			if "shard" in profile_infos:
				shard = profile_infos["shard"]
			else:
				shard = filter_shard

			if shard.lower() in self.userInfos and "game_access" in self.userInfos and "name" in self.userInfos:
				use_name = shard+"_"+self.userInfos["name"]
				if filter_profile == "profile_"+use_name:
					profile_name = shard+"_"+self.userInfos["name"]
					login = self.userInfos["game_access"]["tokenA"]
					password = self.userInfos["game_access"]["tokenB"]
					args = "{} {} ".format(login, password)
					if not skip_disabled or self.config.get("shortcut_"+profile_name+"_all_chars", "disabled") != "1":
						shortcuts["shortcut_"+profile_name+"_all_chars"] = ("*", args, self.config.get("shortcut_"+profile_name+"_all_chars", "disabled") == "1")
					userInfos = self.userInfos[shard.lower()]
					if userInfos:
						for i, char in enumerate(self.userInfos[shard.lower()]):
							si = "shortcut_"+profile_name+"_"+str(i)
							if char and (not skip_disabled or self.config.get(si, "disabled") != "1"):
								shortcuts[si] = (char, args+"slot {}".format(i), self.config.get(si, "disabled") == "1")
			return shortcuts
		else:
			for shard in ("Atys", "Yubo", "Gingo"):
				if shard.lower() in self.userInfos and "name" in self.userInfos and "game_access" in self.userInfos:
					login = self.userInfos["game_access"]["tokenA"]
					password = self.userInfos["game_access"]["tokenB"]
					args = "{} {} ".format(login, password)
					profile_name = shard+"_"+self.userInfos["name"]
					if not profile_name in self.shortcuts:
						self.shortcuts[profile_name] = {}
					if not "shortcut_"+profile_name+"_all_chars" in self.shortcuts[profile_name]:
						si = "shortcut_"+profile_name+"_all_chars"
						self.config.set(si, "name", "*")
						self.config.set(si, "profile", profile_name)
						self.config.set(si, "args", args)
						self.config.set(si, "disabled", "0")
						save_config = True
						self.shortcuts[profile_name]["shortcut_"+profile_name+"_all_chars"] = ("*", args , False)
					userInfos = self.userInfos[shard.lower()]
					if userInfos:
						for i, char in enumerate(userInfos):
							si = "shortcut_"+profile_name+"_"+str(i)
							if si in self.shortcuts[profile_name]:
								if not char:
									self.config.remove(si)
									save_config = True
								else:
									if self.shortcuts[profile_name][si][0] != char:
										self.config.set(si, "name", char)
										save_config = True
										self.shortcuts[profile_name][si][0] = char
							elif char : 
								self.config.set(si, "name", char)
								self.config.set(si, "profile", "profile_"+shard+"_"+self.userInfos["name"])
								self.config.set(si, "args", args+"slot {}".format(i))
								self.config.set(si, "disabled", "0")
								save_config = True
								self.shortcuts[profile_name][si] = (char, args+"slot {}".format(i), False)
		if save_config:
			self.config.save()
		return self.shortcuts

	def addDefaultProfile(self, pi, name, shard):
		i = 0
		correct_profile_shard = False
		folder = "{}".format(i)
		while True:
			if p.isdir(self.ryzom_roaming_path+os.sep+folder):
				domain = self.getDomainFromClientCfg(self.ryzom_roaming_path+os.sep+folder+os.sep+"client.cfg")
				self.log("Have folder with domain: {}".format(domain))
				if domain:
					if shard == self.getShardName(domain):
						correct_profile_shard = True
						break
			else:
				break
			i += 1
			folder = "{}".format(i)

		self.config.set(pi, "name", name)
		self.config.set(pi, "shard", shard)
		self.config.set(pi, "folder", folder)
		self.log("********** SAVE ************** "+folder+" "+shard)
		self.config.save()

		if not correct_profile_shard:
			self.createProfileFolder(folder)
			self.updateShardInProfile(shard, folder);

		return folder

	def createProfileFolder(self, folder):
		if not p.isdir(self.ryzom_roaming_path+os.sep+folder):
			os.makedirs(self.ryzom_roaming_path+os.sep+folder)

	def updateShardInProfile(self, shard, folder):
		self.log("Update {}".format(shard))
		servers = self.getServersFromIni()
		if shard in servers:
			root_location = servers[shard]
		else:
			root_location = self.ryzom_roaming_path+os.sep+"Ryzom"+os.sep+"ryzom_live"

		RyzomCfg = self.getModule("ryzom_cfg")
		cfg = RyzomCfg.getCfg(k.join(self.ryzom_roaming_path, folder, "client.cfg"), k.join(root_location, "client_default.cfg"))
		cfg.set("Application", [self.getDomainName(shard), "./client_ryzom_r.exe", "."])
		cfg.save()
	
	def updateLangInProfile(self, shard, lang, folder):
		self.log("Update {}".format(lang))
		servers = self.getServersFromIni()
		if shard in servers:
			root_location = servers[shard]
		else:
			root_location = self.ryzom_roaming_path+os.sep+"Ryzom"+os.sep+"ryzom_live"
			
		RyzomCfg = self.getModule("ryzom_cfg")
		cfg = RyzomCfg.getCfg(k.join(self.ryzom_roaming_path, folder, "client.cfg"), k.join(root_location, "client_default.cfg"), True)
		cfg.set("LanguageCode", lang)
		cfg.save()

	def call_Close(self):
		self.hideE("popup")

	def call_SelectRyzomPath(self, shard):
		file_types = ("All Files (*.*)", )
		result = self.openFolderDialog()
		if result and len(result) > 0:
			selected_path = result[0]
		else:
			selected_path = self.getE("#-"+shard+"-path")["value"]
			if not selected_path:
				selected_path = self.ryzom_user_path
		self.setupE("#-"+shard+"-path", "value", "\""+selected_path.replace("\\", "\\\\").replace("\"", "\\\"")+"\"")
		versions = self.getDomainVersionsFromPath(selected_path)
		self.updatePathInformations(shard, selected_path, versions)
	
	def call_ValidateRyzomPaths(self):
		for server in ("Atys", "Yubo", "Gingo"):
			server_path = self.getE("#-"+server.lower()+"-path")
			if server_path and "value" in server_path:
				server_path = server_path["value"]
				if not p.isdir(server_path):
					os.makedirs(server_path)
				self.config.set(server, "location", server_path)
		self.config.save()
		self.getModule("ryzom_content_manager").call_ShowRyzomPlay()
		self.getModule("ryzom_installer").setup()

	def call_ValidateRyzomProfiles(self):
		for name, profile in self.selected_profiles.items():
			if profile["name"][0] != "/" or not profile["disabled"]:
				self.config.set(name, "id", profile["id"])
				self.config.set(name, "name", profile["name"])
				self.config.set(name, "disabled", "1" if profile["disabled"] else "0")
				self.config.set(name, "shard", profile["shard"])
				self.config.set(name, "args", profile["args"])
		self.config.save()
		self.setup()
	
	def call_DisplayConfigServers(self):
		userInfos = self._getUserInfos()
		ryzom_team = False
		if userInfos:
			ryzom_team = "yubo" in userInfos and userInfos["yubo"] != None
			
		profiles = self.getProfilesFromIni()
		if "Yubo" in profiles:
			ryzom_team = "yubo"

		content = self.getTemplateV2("found_servers", {"ryzom_team?": ryzom_team})
		
		self.setZone("main", self.getTemplateV2("configuration", {"selected_servers": "selected", "title": self._("manage_your_ryzom_install"), "content": content}))
		self.need_setup_main = True

		servers = self.getServersFromIni()
		ryzom_intallation_path = ""
		ryzom_default_path = self.ryzom_user_path
		if not servers:
			if p.isfile(ryzom_default_path+os.sep+"ryzom_installer.ini"):
				self.log("ryzom_installer.ini found at {}".format(ryzom_default_path))
				config = KyssConfigIni(ryzom_default_path+os.sep+"ryzom_installer.ini")
				ryzom_intallation_path = config.get("common", "installation_directory")
				if ryzom_intallation_path:
					ryzom_intallation_path = ryzom_intallation_path.replace("\\", os.sep).replace("/", os.sep)
		
			if ryzom_intallation_path:
				self.setMessage(self._("installation_found_in", (ryzom_intallation_path.replace("\\", "\\\\")))+"<br />")
			else:
				self.setMessage(self._("choose_ryzom_install_dir")+"<br />")
		else:
			self.setMessage(self._("changing_ryzom_install")+"<br />")

		if not ryzom_intallation_path:
			ryzom_intallation_path = ryzom_default_path
		
		used_servers = ["Atys", "Yubo", "Gingo"]
		for server in used_servers:
			if servers and server in servers and servers[server]:
				path = servers[server]
			else:
				path = ryzom_intallation_path+os.sep+self.getDomainName(server)
				self.config.set(server, "location", path)
				self.config.save()

			if not p.isdir(path):
				path = ryzom_default_path+os.sep+self.getDomainName(server)
			self.log("PATH "+path)
			versions = self.getDomainVersionsFromPath(path)
			self.setupE("#-"+server.lower()+"-path", "value", "\""+path.replace("\\", "\\\\")+"\"")
			self.updatePathInformations(server.lower(), path, versions)
		

	def call_DisplayConfigProfiles(self, arg=""):
		self.selected_profiles = {}

		message = ""
		profiles = self.getProfilesFromIni()
		shortcuts = self.getShortcutsFromIni()

		found_profiles = []
		if profiles:
			for shard, shard_profiles in profiles.items():
				for profile_id, profile in shard_profiles.items():
					found_profiles.append({"__id__": profile_id, "name": profile[0], "shard": shard, "folder": self.esc(profile[1])})
		profiles_html = ""
		i = 0
		for found_profile in found_profiles:
			self.selected_profiles[found_profile["__id__"]] = found_profile
			if p.isdir(self.ryzom_roaming_path+os.sep+found_profile["folder"]):
				found_profile["folder_link?"] = {"folder": self.posixPath(self.ryzom_roaming_path)+"/"+found_profile["folder"], "name": found_profile["folder"]}
				found_profile["domain"] = self.getDomainFromClientCfg(self.ryzom_roaming_path+os.sep+found_profile["folder"]+os.sep+"client.cfg")
			else:
				found_profile["folder_break?"] = {"name": found_profile["folder"]}
				found_profile["domain"] = "ryzom_live"
			
			if found_profile["__id__"] in shortcuts:
				total_shortcuts = len(shortcuts[found_profile["__id__"]])
			else:
				total_shortcuts = 0
			
			found_profile["deletable?"] = total_shortcuts == 0
			
			found_profile["total_shortcuts"] = total_shortcuts
			found_profile["cycle"] = "odd" if i % 2 else "even"
			i += 1
			
			found_profile["shard"] = self.getShardName(found_profile["domain"])
			profiles_html += self.getTemplateV2("profile", found_profile)


		content = self.getTemplateV2(
			"found_profiles",
			{	"profiles_location": self.posixPath(self.ryzom_roaming_path),
				"profiles": profiles_html,
				"profiles_infos" : self._("profiles_infos").replace("\n", "<br />")})
		self.setZone("main", self.getTemplateV2("configuration", {"selected_profiles": "selected", "content": content}))
		self.setMessage(message)

	def call_DisplayConfigShortcuts(self):
		self.need_setup_main = True
		self.selected_shortcuts = {}

		message = ""
		profiles = self.getProfilesFromIni()
		shortcuts = self.getShortcutsFromIni()

		found_shortcuts = []
		for profile, profile_shortcuts in shortcuts.items():
			for shortcut_id, shortcut in profile_shortcuts.items():
				found_shortcuts.append({"__id__": shortcut_id, "name": shortcut[0], "profile": profile, "args": self.esc(shortcut[1]), "disabled": shortcut[2]})
		
		shortcuts_htmlA = ""
		shortcuts_htmlB = ""
		iA = 0
		iB = 0
		
		for found_shortcut in found_shortcuts:
			self.selected_shortcuts[found_shortcut["__id__"]] = found_shortcut
			profile_args = []
			args = found_shortcut["args"]
			arg_i = 0
			for arg in args.split(" "):
				if arg_i == 1:
					profile_args.append("*****")
				else:
					profile_args.append(arg)
				arg_i += 1
			
			profile_shard = ""
			found_shortcut["args"] = " ".join(profile_args)
			for shard, shard_profiles in profiles.items():
				if found_shortcut["profile"] in shard_profiles:
					profile_folder = shard_profiles[found_shortcut["profile"]][1]
					profile_shard = shard
					profile_name = shard_profiles[found_shortcut["profile"]][0]
			
			if not profile_shard:
				continue
			found_shortcut["shard"] = profile_shard
			if p.isdir(self.ryzom_roaming_path+os.sep+profile_folder):
				found_shortcut["folder_link?"] = {"folder": self.posixPath(self.ryzom_roaming_path)+"/"+profile_folder, "name": profile_name}
				found_shortcut["domain"] = self.getDomainFromClientCfg(self.ryzom_roaming_path+os.sep+profile_folder+os.sep+"client.cfg")
			else:
				found_shortcut["folder_break?"] = {"name": profile_name}

			found_shortcut["remove?"] = found_shortcut["__id__"][0] != "_"
			if found_shortcut["disabled"]:
				found_shortcut["cycle"] = "odd" if iB % 2 else "even"
				found_shortcut["enable?"] = {"id": found_shortcut["__id__"]}
				found_shortcut["disabled"] = "disabled"
				iB += 1
				shortcuts_htmlB += self.getTemplateV2("shortcut", found_shortcut)
			else:
				found_shortcut["cycle"] = "odd" if iA % 2 else "even"
				found_shortcut["disable?"] = {"id": found_shortcut["__id__"]}
				iA += 1
				shortcuts_htmlA += self.getTemplateV2("shortcut", found_shortcut)
			found_shortcut["args"] = args
		content = self.getTemplateV2("found_shortcuts", {"shortcuts": shortcuts_htmlA+shortcuts_htmlB, "shortcuts_infos" : self._("shortcuts_infos").replace("\n", "<br />")})
		self.setZone("main", self.getTemplateV2("configuration", {"selected_shortcuts": "selected", "content": content}))
		self.setMessage(message)

	def call_EditProfile(self, profile_id=""):
		self.setMessage("")
		if profile_id:
			profile = self.selected_profiles[profile_id]
		else:
			i = len(self.selected_profiles)-1
			folder = "{}".format(i)
			while p.isdir(self.ryzom_roaming_path+os.sep+folder):
				i += 1
				folder = "{}".format(i)
				
			profile = {"name": "", "folder": folder}
			profile["new?"] = True

		profile["__id__"] = profile_id
		profile["shards"] = self.templatize({"Atys": "Atys", "Yubo": "Yubo", "Gingo": "Gingo"})
		self.setE("popup", self.getTemplateV2("edit_profile", profile))
		self.showE("popup")



	def call_AskDeleteProfile(self, args):
		profile_id, profile_name = args
		self.setE("popup", self.getTemplateV2("/ask", {
			"question": "Do you want really remove the profile {}?".format("<span class='bold orange'>"+profile_name+"</span>"),
			"yes": "YES",
			"no": "NO",
			"module": "ryzom_config_ini",
			"action": "DeleteProfile",
			"arg": profile_id,
			}))
		self.showE("popup")


	def call_DeleteProfile(self, args):
		profile_id, confirm = args
		if confirm == "1":
			self.config.remove(profile_id)
			self.config.save()
		self.hideE("popup")
		self.call_DisplayConfigProfiles("")



	def call_ValidateEditProfile(self, profile_id):
		def checkFolder(name):
			if not name:
				return False
			for c in name:
				oc = ord(c)
				if not ((oc >= ord("a") and oc <= ord("z")) or (oc >= ord("A") and oc <= ord("Z")) or (oc >= ord("0") and oc <= ord("9")) or c == "_"):
					return False
			
			if p.isdir(self.ryzom_roaming_path+os.sep+name):
				return False
			return True
			
		name = self.getE("#-edit-name", "value")
		shard = self.getE("#-edit-shard", "value")
		if not name:
			self.setMessage("Invalid name of profile")
		else:
			if not profile_id:
				folder = self.getE("#-edit-folder", "value")
				if not checkFolder(folder):
					self.setMessage("Invalid folder of profile")
					self.hideE("popup")
					return
				i = len(self.selected_profiles)-1
				profile_id = "profile_{}".format(i)
				while profile_id in self.selected_profiles:
					i += 1
					profile_id = "profile_{}".format(i)
				self.config.set(profile_id, "folder", folder)
			
			folder = self.config.get(profile_id, "folder")
			#profile_shard = self.config.get(profile_id, "shard")
			servers = self.getServersFromIni()
			if shard in servers:
				root_location = servers[shard]
			else:
				root_location = self.ryzom_roaming_path+os.sep+"Ryzom"+os.sep+"ryzom_live"
			
			cfgname = self.ryzom_roaming_path+os.sep+folder+os.sep+"client.cfg"
			if os.path.isfile(cfgname):
				ryzomCfg = self.getModule("ryzom_cfg").getCfg(cfgname, root_location+os.sep+"client_default.cfg")
			else:
				self.createProfileFolder(folder)
			
			self.updateShardInProfile(shard, folder);

			self.config.set(profile_id, "name", name)
			self.config.save()
			self.call_DisplayConfigProfiles()
		
		self.hideE("popup")

	def call_EnableDisableShortcut(self, shortcut_id):
		img = self.getE("#-shortcut-{}".format(shortcut_id))
		disable =  img["src"].split("/")[-1] == "tick_on.png"
		if shortcut_id[0] == "_" and not disable:
			self.config.remove(shortcut_id)
		else:
			self.config.set(shortcut_id, "disabled", "1" if disable else "0")
		self.config.save()
		self.call_DisplayConfigShortcuts()


	def call_AskRemoveShortcut(self, args):
		shortcut_id, shorcut_name = args
		self.setE("popup", self.getTemplateV2("/ask", {
			"question": "Do you want really remove the shortcut {}?".format("<span class='bold orange'>"+shorcut_name+"</span>"),
			"yes": "YES",
			"no": "NO",
			"module": "ryzom_config_ini",
			"action": "RemoveShortcut",
			"arg": shortcut_id,
			}))
		self.showE("popup")


	def call_RemoveShortcut(self, args):
		shortcut_id, confirm = args
		if confirm == "1":
			self.config.remove(shortcut_id)
			self.config.save()
		self.hideE("popup")
		self.call_DisplayConfigShortcuts()


	def call_AddShortcutToDesktop(self, args):
		pass


	def call_EditShortcut(self, shortcut_id=""):
		self.setMessage("")
		slot = ""
		if shortcut_id:
			shortcut = self.selected_shortcuts[shortcut_id]
			shortcut["Profile"] = shortcut["profile"][0].upper()+shortcut["profile"][1:]
			args = shortcut["args"].split(" ", 3)
			name = ""
			password = ""
			if args[0] and args[0][0] != "/":
				name = args[0]
				if len(args) > 1 and args[1]:
					if args[1][0] != "/":
						password = args[1]
						if len(args) > 3:
							slot = args[3]
		
			shortcut["argname"] = name
			shortcut["argpassword"] = password
			shortcut["__id__"] = shortcut_id
			shortcut["new?"] = True
		else:
			shortcut = {"profile": "", "shard": ""}

		profiles = {}
		for shard, profiles_list in self.getProfilesFromIni().items():
			if not shortcut["shard"] or shard == shortcut["shard"]:
				for pid, infos in profiles_list.items():
					if not "@" in infos[0]:
						profiles[pid] = [shard+" / "+infos[0], infos[1]]

		final_profiles = []
		for pid, profile in profiles.items():
			final_profiles.append({"id": pid, "name": profile[0], "selected": "selected='selected'" if pid == shortcut["profile"] else ""})

		shortcut["profiles"] = final_profiles
		shortcut["new?"] = True
		if slot:
			shortcut["slot{}?".format(slot)] =  True
		
		self.setE("popup", self.getTemplateV2("edit_shortcut", shortcut))
		self.showE("popup")

	def call_ValidateEditShortcut(self, shortcut_id):
		name = self.getE("#-edit-name", "value")
		if not name:
			self.setMessage("Invalid name of shortcut")
		else:
			profile = self.getE("#-edit-profile", "value")
			slot = self.getE("#-edit-argslot", "value")
			if slot:
				args = [self.getE("#-edit-argname", "value"), self.getE("#-edit-argpassword", "value"), "slot", slot]
			else:
				args = [self.getE("#-edit-argname", "value"), self.getE("#-edit-argpassword", "value")]
			args = " ".join(args).replace("  ", " ")
			if not shortcut_id:
				i = len(self.selected_shortcuts)-1
				shortcut_id = "shortcut_{}".format(i)
				while shortcut_id in self.selected_shortcuts:
					i += 1
					shortcut_id = "shortcut_{}".format(i)
				
			self.config.set(shortcut_id, "name", name)
			self.config.set(shortcut_id, "profile", profile)
			self.config.set(shortcut_id, "args", args)
			self.config.set(shortcut_id, "disabled", "0")
			self.config.save()
			self.call_DisplayConfigShortcuts()
		
		self.hideE("popup")

	def importProfilesAndShortcuts(self):
		# Check ryzom_installer.ini
		profiles = {}
		if p.isfile(self.ryzom_roaming_path+os.sep+"ryzom_installer.ini"):
			self.log("ryzom_installer.ini found at {}".format(self.ryzom_roaming_path))
			config = KyssConfigIni(self.ryzom_roaming_path+os.sep+"ryzom_installer.ini")
			self.log("Config = {}".format(config))
			if config.get("profiles", "size"):
				profiles_size = int(config.get("profiles", "size"))
			else:
				profiles_size = 0
			self.log("Found {} profiles: ".format(profiles_size))
			for i in range(profiles_size):
				profile_id = "profile_{}".format(i)
				profile_folder = config.get(profile_id, "id")
				profile_name = config.get(profile_id, "name")
				profile_args = config.get(profile_id, "arguments")
				new_id = "profile_"+profile_folder
				config_file = p.join(self.ryzom_roaming_path, profile_folder, "client.cfg")
				domain = self.getDomainFromClientCfg(config_file, None)
				if domain == None:
					domain = config.get(profile_id, "server")

				shard = self.getShardName(domain)

				if not new_id in profiles:
					profiles[new_id] = profile_id
					self.config.set(new_id, "name", profile_name)
					self.config.set(new_id, "shard", shard)
					self.config.set(new_id, "folder", profile_folder)
					self.createProfileFolder(profile_folder)
					self.updateShardInProfile(shard, profile_folder)
					
				
				self.config.set("shortcut_{}".format(i), "name", profile_name)
				self.config.set("shortcut_{}".format(i), "profile", new_id)
				self.config.set("shortcut_{}".format(i), "args", profile_args)
				self.config.set("shortcut_{}".format(i), "disabled", "0")

		self.config.save()

	def setup(self, display=""):
		if not self.config.fileExists:
			self.importProfilesAndShortcuts()
			
		## Load saved servers and profiles from ryzom.ini
		self.log("Getting servers...")
		servers = self.getServersFromIni()
		self.log("Getting profiles...")
		profiles = self.getProfilesFromIni()
		self.log("Getting shortcuts...")
		shortcuts = self.getShortcutsFromIni()
		self.log("We have {} servers:\n{} {}".format(len(servers), self.color("cyan"), servers))
		self.log("We have {} profiles:\n{} {}".format(len(profiles), self.color("cyan"), profiles))
		
		if display == "servers":
			self.call_DisplayConfigServers()
		elif display == "profiles":
			self.call_DisplayConfigProfiles()
		else:
			self.call_DisplayConfigShortcuts()
		
		KyssModule.setup(self)
